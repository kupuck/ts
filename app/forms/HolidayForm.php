<?php

namespace Tracking\Forms;

use Phalcon\Forms\Element\Check;
use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\Date as DateElement;
use Phalcon\Forms\Element\Submit;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Date as DateValidator;

class HolidayForm extends Form
{
    public function initialize()
    {
        $holiday = new Text('name',
            [
                'placeholder' => 'Name of the Holiday',
                'class' => 'form-control'
            ]
        );
        $holiday->addValidators(
            [
                new PresenceOf(['message' => 'The name of Holiday is required'])
            ]
        );
        $this->add($holiday);

        $date = new DateElement('date', ['class' => 'form-control']);
        $date->setLabel('Date:');
        $date->addValidators(
            [
                new PresenceOf(
                    ['message' => 'The start date of holiday is required']
                ),
                new DateValidator(
                [
                    'format' => 'Y-m-d',
                    'message' => "The start date is invalid"
                ])
            ]
        );
        $this->add($date);

        $this->add(new Submit('Save', ['class' => 'btn btn-outline-success']));
    }
}