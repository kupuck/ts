<?php

namespace Tracking\Forms;

use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\Check;
use Phalcon\Forms\Element\Select;
use Phalcon\Forms\Element\Submit;
use Phalcon\Forms\Element\Hidden;
use Phalcon\Forms\Element\Password;
use Phalcon\Validation\Validator\Email;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\StringLength;
use Phalcon\Validation\Validator\Confirmation;
use Phalcon\Validation\Validator\Identical;

class UserForm extends Form
{
    public function initialize()
    {
        $username = new Text('username',
            [
                'placeholder' => 'Username',
                'class' => 'form-control'
            ]
        );
        $username->addValidator(
                new PresenceOf(['message' => 'The name of Holiday is required'])
        );
        $this->add($username);


        $password = new Password('password',
            [
                'placeholder' => 'Password',
                'class' => 'form-control'
            ]
        );
        $password->addValidators([
            new PresenceOf([
                'message' => 'The password is required'
            ]),
            new StringLength([
                'min' => 8,
                'messageMinimum' => 'Password is too short. Minimum 8 characters'
            ]),
            new Confirmation([
                'message' => 'Password doesn\'t match confirmation',
                'with' => 'confirm'
            ])
        ]);
        $this->add($password);


        $confirm = new Password('confirm',
            [
                'placeholder' => 'Confirm password',
                'class' => 'form-control'
            ]
        );
        $confirm->addValidator(
            new PresenceOf([
                'message' => 'The confirmation password is required'
            ])
        );
        $this->add($confirm);

        $email = new Text('email',
            [
                'placeholder' => 'E-Mail',
                'class' => 'form-control'
            ]
        );
        $email->addValidators(
            [
                new PresenceOf(['message' => 'The confirmation password is required']),
                new Email(['message' => 'The e-mail is not valid'])
            ]
        );
        $this->add($email);

        $role = new Select('role',
            [
                'user' => 'User',
                'admin' => 'Administrator'
            ]
        );

        $role->addValidator(
            new PresenceOf([
                'message' => 'The role is required'
            ])
        );
        $this->add($role);

        $csrf = new Hidden('csrf');
        $csrf->addValidator(new Identical([
            'value' => $this->security->getSessionToken(),
            'message' => 'CSRF validation failed'
        ]));
        $csrf->clear();
        $this->add($csrf);

        $this->add(new Submit('Save', ['class' => 'btn btn-outline-success']));
    }
}