<?php

namespace Tracking\Forms;

use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\Password;
use Phalcon\Forms\Element\Hidden;
use Phalcon\Forms\Element\Submit;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Identical;

class LoginForm extends Form
{
    public function initialize()
    {
        $username = new Text('username',
            [
                'placeholder' => 'Username',
                'class' => 'form-control'
            ]
        );
        $username->addValidators(
            [
                new PresenceOf(['message' => 'The username is required'])
            ]
        );
        $this->add($username);

        $password = new Password('password',
            [
                'placeholder' => 'Password',
                'class' => 'form-control'
            ]
        );
        $password->addValidator(new PresenceOf(['message' => 'The password is required']));
        $password->clear();
        $this->add($password);

        $csrf = new Hidden('csrf');
        $csrf->addValidator(new Identical(
            [
                'value' => $this->security->getSessionToken(),
                'message' => 'CSRF validation failed'
            ]
        ));
        $csrf->clear();
        $this->add($csrf);
        $this->add(new Submit('Submit', ['class' => 'btn btn-outline-success form-control']));
    }
}