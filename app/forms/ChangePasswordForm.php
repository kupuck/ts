<?php

namespace Tracking\Forms;

use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Submit;
use Phalcon\Forms\Element\Password;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Confirmation;
use Phalcon\Validation\Validator\StringLength;

class ChangePasswordForm extends Form
{
    public function initialize()
    {
        $password = new Password('password',
            [
                'placeholder' => 'Password',
                'class' => 'form-control'
            ]
        );
        $password->addValidators([
            new PresenceOf([
                'message' => 'The password is required'
            ]),
            new StringLength([
                'min' => 8,
                'messageMinimum' => 'Password is too short. Minimum 8 characters'
            ]),
            new Confirmation([
                'message' => 'Password doesn\'t match confirmation',
                'with' => 'confirm'
            ])
        ]);
        $this->add($password);


        $confirm = new Password('confirm',
            [
                'placeholder' => 'Confirm password',
                'class' => 'form-control'
            ]
        );
        $confirm->addValidator(
            new PresenceOf([
                'message' => 'The confirmation password is required'
            ])
        );
        $this->add($confirm);

        $this->add(new Submit('Change', ['class' => 'btn btn-outline-success']));
    }
}