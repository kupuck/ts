<?php

namespace Tracking\Models;

class Additional
{
    public static function getAllDaysInMonth($month, $year)
    {
        $result = [];
        $start = strtotime('01-' . $month . '-' . $year);
        $end = strtotime('+1 month', $start);

        for($i = $start, $j = 0; $i < $end; $i += 86400, $j++){
            $result[$j]['day'] = date('D', $i);
            $result[$j]['number'] = date('d', $i);
        }

        if($month == date('m') && $year == date('Y')) {
            $current = (int)date('d');
            array_unshift($result, $result[$current-1]);
            unset($result[$current]);
            $result = array_values($result);
        }

        return $result;
    }

    public static function isTime($time)
    {
        $check = \DateTime::createFromFormat('H:i:s', $time);

        return $check && $check->format('H:i:s') === $time;
    }
}