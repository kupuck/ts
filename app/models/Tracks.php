<?php

namespace Tracking\Models;

use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Resultset\Simple as Resultset;

class Tracks extends Model
{
    public function initialize()
    {
        $this->hasMany('id', __NAMESPACE__ . '\TracksTime', 'track_id',
            [
                'alias' => 'tracks_time',
                'foreignKey' => [
                    'message' => 'Track cannnot be delete, because it\'s used on TracksTime'
                ]
            ]
        );

        $this->belongsTo('user_id', __NAMESPACE__ . '\Users', 'id',
            [
                'alias' => 'user'
            ]
        );
    }

    public static function updateTotal($track_id)
    {
        $total = 0;

        $track_times = TracksTime::find([
            'track_id = :track_id:',
            'bind' => ['track_id' => $track_id]
        ]);

        foreach ($track_times as $time)
            $total += strtotime($time->end_time) - strtotime($time->start_time);

        $track = Tracks::findFirst([
            'id = :id:',
            'bind' => ['id' => $track_id]
        ]);
        $track->total = $total;
        $track->save();

        return $total;
    }

    public static function getDailyByUser($user_id, $date = null)
    {
        if ($date == null)
            $date = date('Y-m-d');

        $tracks = self::find(
            [
                'user_id = :user_id: AND date = :date:',
                'bind' => [
                    'user_id' => $user_id,
                    'date' => $date
                ]
            ]
        );

        if (count($tracks) == 0) {
            $tracks = new Tracks([
                'user_id' => $user_id,
                'date' => $date
            ]);
            $tracks->save();
            $tracks = self::find(
                [
                    'user_id = :user_id: AND date = :date:',
                    'bind' => [
                        'user_id' => $user_id,
                        'date' => $date
                    ]
                ]
            );
        }

        return $tracks->toArray();
    }

    public static function getMonthAssigned($month, $year)
    {
        $month = (strlen($month) == 1) ? '0' . $month : $month;

        $work_days = 0;
        $weekend = ['Sat', 'Sun'];
        $start = strtotime('01-'.$month.'-'.$year);
        $end = strtotime('+1 month', $start);

        $holidays = Holidays::find([
            'columns' => ['date', 'every_year'],
            "date LIKE :date: OR date LIKE :month: AND every_year = 1",
            'bind' => [
                'date' => $year . '-' . $month . '%',
                'month' => '%-' . $month . '-%'
            ]
        ])->toArray();

        //Проверка на выходные
        for ($i = $start; $i < $end; $i += 86400)
            if (!(in_array(date('D', $i), $weekend)))
                $work_days++;

        //Проверка на праздники
        foreach($holidays as $holiday)
            for ($i = $start; $i < $end; $i += 86400)
                if (strpos($holiday['date'], date('m-d', $i)) && $holiday['date'] == date('Y-m-d', $i))
                    $work_days--;

        return $work_days * 9;
    }

    public static function getMonthTotalByUser($id, $month, $year)
    {
        $result = 0;
        $month = (strlen($month) == 1) ? '0' . $month : $month;
        $exp = $year . '-' . $month . '%';

        $tracks = Tracks::find([
            'user_id = :user_id: AND date LIKE :date:',
            'bind' => ['user_id' => $id, 'date' => $exp]
        ]);

        foreach($tracks as $track)
            $result += (int)$track->total;
        $result = floor($result / 3600) . ':' . floor(($result / 60) % 60);

        return $result;
    }

    public static function getMonthFailsByUser($id, $month, $year)
    {
        $result = 0;
        $month = (strlen($month) == 1) ? '0' . $month : $month;
        $exp = $year . '-' . $month .'%';

        $tracks = Tracks::find([
            'user_id = :user_id: AND date LIKE :date: AND total != 0',
            'bind' => ['user_id' => $id, 'date' => $exp]
        ])->toArray();

        foreach($tracks as $track)
            $result += (int)self::getLateTime($track['id']);

        $result = floor($result / 3600) . ':' . floor(($result / 60) % 60);

        return $result;
    }

    public static function getAvailableYears()
    {
        $tracks = new Tracks();
        $sql = 'SELECT DISTINCT EXTRACT(YEAR FROM `date`) AS year FROM `tracks`';

        $rs = new Resultset(null, $tracks, $tracks->getReadConnection()->query($sql));

        return $rs->toArray();
    }

    public static  function getAvailableMonthsByYear($year)
    {
        $tracks = new Tracks();
        $sql = 'SELECT DISTINCT EXTRACT(MONTH FROM `date`) AS `month` ' .
                'FROM `tracks` ' .
                'WHERE `date` LIKE \'' . $year . '%\'';

        $rs = new Resultset(null, $tracks, $tracks->getReadConnection()->query($sql));
        $months = $rs->toArray();
        unset($rs);

        foreach($months as &$item) {
            $item['number'] = $item['month'];
            $item['name'] = \DateTime::createFromFormat('!m', $item['number'])->format('F');
            unset($item['month']);
        }

        return $months;
    }

    public static function isStopped($user_id)
    {
        $track_id = Tracks::findFirst(
            [
                'user_id = :user_id: AND date = :date:',
                'columns' => 'id',
                'bind' => [
                    'user_id' => $user_id,
                    'date' => date('Y-m-d')
                ]
            ]
        )->id;

        $time = TracksTime::findFirst(
            [
                'columns' => 'id, start_time, end_time',
                'track_id = :track_id:',
                'order' => 'id DESC',
                'bind' => ['track_id' => $track_id]
            ]
        );

        if(isset($time->id, $time)) {
            if($time->end_time != '')
                return ['stop' => true, 'track_id' => $track_id];
            else
                return ['stop' => false, 'track_id' => $track_id];
        }

        return $track_id;
    }

    public static function startTime($track_id)
    {
        $start_time = date('H:i:s');

        $new = new TracksTime([
            'track_id' => $track_id,
            'start_time' => $start_time,
            'end_time' => ''
        ]);
        $new->save();

        $result['stop'] = false;
        $result['time'] = $start_time;
        $result['track_id'] = $track_id;
        $result['user_id'] = Tracks::findFirst([
            'id = :id:',
            'bind' => ['id' => $track_id]
        ])->user_id;

        return $result;
    }

    public static function stopTime($track_id)
    {
        $end_time = date('H:i:s');

        $time = TracksTime::findFirst(
            [
                'track_id = :track_id:',
                'order' => 'id DESC',
                'bind' => ['track_id' => $track_id]
            ]
        );
        $time->end_time = $end_time;
        $time->save();

        $result['stop'] = true;
        $result['time'] = $end_time;
        $result['track_id'] = $track_id;
        $result['total'] = gmdate('H:i', Tracks::updateTotal($time->track_id));
        $result['user_id'] = Tracks::findFirst([
            'id = :id:',
            'bind' => ['id' => $track_id]
        ])->user_id;

        return $result;
    }

    public static function getLateTime($track_id)
    {
        $time = TracksTime::findFirst([
            'track_id = :track_id:',
            'bind' => ['track_id' => $track_id]
        ]);

        if($time) {
            $work_start_time = Settings::findFirst([
                "name = 'work_start_time'"
            ])->value;

            $difference = strtotime($work_start_time) - strtotime($time->start_time);
            if ($difference < 0)
                return abs($difference);
        }

        return 0;
    }
}