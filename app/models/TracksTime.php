<?php

namespace Tracking\Models;

use Phalcon\Mvc\Model;

class TracksTime extends Model
{
    public function initialize()
    {
        $this->belongsTo('track_id', __NAMESPACE__ . '\Tracks', 'id',
            [
                'alias' => 'track'
            ]
        );
    }
}