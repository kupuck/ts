<?php

namespace Tracking\Models;

use Phalcon\Mvc\Model;

class Users extends Model
{
    public function initialize()
    {
        $this->hasMany('id', __NAMESPACE__ . '\Tracks', 'user_id',
            [
                'alias' => 'tracks',
                'foreignKey' => [
                    'message' => 'Track cannnot be delete, because it\'s used on Tracks'
                ]
            ]
        );
    }

    public static function getLateUsers($date = null)
    {
        if($date == null)   $date = date('Y-m-d');

        $tracks = Tracks::find([
            'date = :date:',
            'bind' => ['date' => $date]
        ])->toArray();


        $laters = [];
        if(count($tracks) > 0) {
            $i = 0;

            foreach($tracks as $track) {
                $late = Tracks::getLateTime($track['id']);

                if($late != '0') {
                    $laters[$i]['id'] = $track['user_id'];
                    $laters[$i]['late_time'] = gmdate('H:i', $late);
                    $i++;
                }
            }

            if(count($laters) > 0) {
                foreach($laters as &$later) {
                    $user = Users::findFirst([
                        'columns' => 'username',
                        'id = :id:',
                        'bind' => ['id' => $later['id']]
                    ]);

                    $later['username'] = $user->username;
                }

                return $laters;
            }
        }

        return 0;
    }
}