{{ content() }}

{{ form() }}
<div class="card container col-3" style="position: center;">
    <div class="card-header row">
        <h4>Create new Holiday</h4>
    </div>
    <div class="card-body">
        <div class="form-group">
            {{ form.render('name') }}
        </div>
        <div class="form-group">
            {{ form.label('date') }}
            {{ form.render('date') }}
        </div>
        {{ form.render('Save') }}
    </div>
</div>
</form>