{{ content() }}


<div class="card container col-6">
    <div class="card-header row">
        <h4>Holidays</h4>&nbsp;&nbsp;&nbsp;&nbsp;
        {{ link_to("holidays/create", "class": "btn btn-outline-dark btn-sm", 'Add new one') }}
    </div>

    <div class="card-body">
        {% for item in holidays %}
            {% if loop.first %}
                <table class="table table-sm table-hover" style="text-align: center;">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Holiday</th>
                        <th>Date</th>
                        <th>Every year</th>
                        <th>Edit</th>
                        <th>Delete</th>
                    </tr>
                    </thead>
            {% endif %}
                    <tbody>
                    <tr>
                        <td>{{ item.id }}</td>
                        <td>{{ item.name }}</td>
                        <td>{{ item.date }}</td>
                        <td>
                            <input id="every_year_{{ item.id }}"
                                   type="checkbox"
                                   class="every_year_checkbox"
                                    {% if item.every_year == 1 %} checked {% endif %}>
                        </td>
                        <td>
                            {{ link_to("holidays/edit/" ~ item.id,
                                '<i class="fa fa-pencil" aria-hidden="true"></i>',
                                "class": "btn btn-sm btn-outline-warning") }}
                        </td>
                        <td>
                            {{ link_to("holidays/delete/" ~ item.id,
                                '<i class="fa fa-times" aria-hidden="true"></i>',
                                "class": "btn btn-sm btn-outline-danger") }}
                        </td>
                    </tr>
            {% if loop.last %}
                </tbody>
            </table>
            {% endif %}
        {% endfor %}
    </div>
</div>

{{ javascriptInclude('js/every_year.js') }}