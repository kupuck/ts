{{ content() }}

<div class="card container col-4">
    <div class="card-header row">
        <h4>Settings</h4>
    </div>

    <div class="card-body">
        {% for item in settings %}
            {% if loop.first %}
                <table class="table table-sm" style="text-align: center;">
                <thead>
                <tr>
                    <th>Option</th>
                    <th>Value</th>
                </tr>
                </thead>
            {% endif %}
            <tbody>
            <tr>
                <td>{{ item['name'] }}</td>
                <td>
                    <input value="{{ item['value'] }}" id="{{ item['name'] }}" class="form-control-sm" type="text">
                </td>
            </tr>
            {% if loop.last %}
                </tbody>
                </table>
            {% endif %}
        {% endfor %}
    </div>
</div>

{{ javascriptInclude('js/change_work_time.js') }}