{{ content() }}


<div class="card container col-6">
    <div class="card-header row">
        <h4>Late users {{ date('Y-m-d') }}</h4>&nbsp;&nbsp;&nbsp;&nbsp;
        {#<div class="form-group">#}
            {#<label for="date">Date:</label>#}
            {#<input type="date" id="date" name="date" class="form-control">#}
        {#</div>#}
        {#{{ link_to("holidays/create", "class": "btn btn-outline-dark btn-sm", 'Add new one') }}#}
    </div>

    <div class="card-body">
        {% if users != 0 %}
            {% for user in users %}
                {% if loop.first %}
                    <table class="table table-sm table-hover" style="text-align: center;">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Username</th>
                        <th>Late time</th>
                    </tr>
                    </thead>
                {% endif %}
                <tbody>
                <tr>
                    <td>{{ user['id'] }}</td>
                    <td>{{ user['username'] }}</td>
                    <td>{{ user['late_time'] }}</td>
                </tr>
                {% if loop.last %}
                    </tbody>
                    </table>
                {% endif %}
            {% endfor %}
        {% endif %}
    </div>
</div>

{{ javascriptInclude('js/every_year.js') }}