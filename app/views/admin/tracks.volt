{{ content() }}

<form action="javascript:0">
    <div class="card container">
        <div class="card-header row">
            <h4>Tracks</h4>&nbsp;&nbsp;&nbsp;&nbsp;
            <div class>
                <select class="custom-select" id="year_select">
                    {% for item in years %}
                        <option value="{{ item['year'] }}" {% if item['year'] in date('Y') %} selected {% endif %}>
                            {{ item['year'] }}
                        </option>
                    {% endfor %}
                </select>
            </div>
            &nbsp;&nbsp;
            <div class>
                <select class="custom-select" id="month_select">
                    {% for month in months %}
                        <option value="{{ month['number'] }}" {% if month['number'] in date('m') %} selected {% endif %}>
                            {{ month['name'] }}
                        </option>
                    {% endfor %}
                </select>
            </div>

            <div class="month-data">
                <strong>You have:</strong> {{ month_data['total'] }}
                <strong>Assign:</strong> {{ month_data['assigned'] }}
                <strong>Fails:</strong>
                {% if month_data['fails'] !== '0:0' %}
                    <span class="fails-text">{{ month_data['fails'] }}</span>
                {% else %}
                    {{ month_data['fails'] }}
                {% endif %}
            </div>

        </div>
        <div class="card-body">
            <table class="table table-bordered table-sm table-responsive" style="text-align: center;">
                <thead>
                <tr>
                    <th scope="col">
                        <p>
                            <a href="javascript:void(0)" id="hideShow" class="btn btn-sm btn-outline-secondary">
                                <i class="fa fa-eye" aria-hidden="true"></i>
                            </a>
                        </p>
                    </th>
                    {% for i in 0..users|length-1 %}
                        <th scope="col" id="{{ users[i]['id'] }}">
                            {{ users[i]['username'] }}
                            <p>
                                <button id="time_{{ users[i]['id'] }}" class="btn btn-outline-success btn-sm time_button">
                                    {{ btn_text[i] }}
                                </button>
                            </p>
                        </th>
                    {% endfor %}
                </tr>
                </thead>
                <tbody>
                {% for i in 1..users[0]['tracks']|length-1 %}
                    {% if loop.first %}
                        <tr class="current_day">
                            <td>
                                <p>{{ users[0]['tracks'][i-1]['day_numb'] }}</p>
                                <p>{{ users[0]['tracks'][i-1]['day'] }}</p>
                            </td>
                            {% for user in users %}
                                <td id="times_{{ user['id'] }}_{{ user['tracks'][i-1]['id'] }}">
                                    <p>
                                            <input type="checkbox" class="lunch"
                                                   id="lunch_{{ user['tracks'][i-1]['id'] }}"
                                                    {% if user['tracks'][i-1]['lunch'] == 1  %} checked {% endif %}>
                                    </p>
                                    {% for times in user['tracks'][i-1]['times'] %}
                                        <p>
                                            <input value="{{ times['start_time'] }}"
                                                   id="start_time_{{ times['id'] }}"
                                                   class="form-control-sm"
                                                   type="text">

                                            <input value="{{ times['end_time'] }}"
                                                   id="end_time_{{ times['id'] }}"
                                                   class="form-control-sm"
                                                   type="text">
                                        </p>
                                    {% endfor %}
                                    <span>
                                        {% if user['tracks'][i-1]['total'] !== '00:00'  %}
                                            <h6>Total: {{ user['tracks'][i-1]['total'] }}</h6>
                                        {% endif %}
                                    </span>
                                    <span>
                                        {% if user['tracks'][i-1]['less'] !== '00:00' and user['tracks'][i-1]['less'] !== '09:00' %}
                                            <h6>Less: {{ user['tracks'][i-1]['less'] }}</h6>
                                        {% endif %}
                                    </span>
                                    <span>
                                        {% if user['tracks'][i-1]['late'] !== '00:00' %}
                                            <h6 class="fails-text">Fail: {{ user['tracks'][i-1]['late'] }}</h6>
                                        {% endif %}
                                    </span>
                                </td>
                            {% endfor %}
                        </tr>
                    {% endif %}
                    <tr class="other_days">
                        <td>
                            <p>{{ users[0]['tracks'][i]['day_numb'] }}</p>
                            <p>{{ users[0]['tracks'][i]['day'] }}</p>
                        </td>
                        {% for user in users %}
                            <td id="times_{{ user['id'] }}_{{ user['tracks'][i]['id'] }}">
                                {% for times in user['tracks'][i]['times'] %}
                                    <p>
                                        <input value="{{ times['start_time'] }}"
                                               id="start_time_{{ times['id'] }}"
                                               class="form-control-sm"
                                               type="text">

                                        <input value="{{ times['end_time'] }}"
                                               id="end_time_{{ times['id'] }}"
                                               class="form-control-sm"
                                               type="text">
                                    </p>
                                {% endfor %}

                                <span>
                                    {% if user['tracks'][i]['total'] !== '00:00'  %}
                                        <h6>Total: {{ user['tracks'][i]['total'] }}</h6>
                                    {% endif %}
                                </span>
                                <span>
                                    {% if user['tracks'][i]['less'] !== '00:00' and user['tracks'][i]['less'] !== '09:00'  %}
                                        <h6>Less: {{ user['tracks'][i]['less'] }}</h6>
                                    {% endif %}
                                </span>
                                <span>
                                    {% if user['tracks'][i]['late'] !== '00:00' %}
                                        <h6 class="fails-text">Fail: {{ user['tracks'][i]['late'] }}</h6>
                                    {% endif %}
                                </span>
                            </td>
                        {% endfor %}
                    </tr>
                {% endfor %}
                </tbody>
            </table>

        </div>
    </div>
</form>


{{ javascriptInclude('js/get_months_by_year.js') }}
{{ javascriptInclude('js/get_data_by_month.js') }}
{{ javascriptInclude('js/control_time.js') }}
{{ javascriptInclude('js/change_time.js') }}
{{ javascriptInclude('js/hide_show.js') }}
{{ javascriptInclude('js/lunch.js') }}