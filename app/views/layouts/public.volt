<nav class="navbar navbar-dark bg-dark navbar-expand-lg">
    {{ link_to(null, 'class': 'navbar-brand', 'Home') }}

    <div class="collapse navbar-collapse">
    {% if not(logged_in['id'] is empty) %}
            <ul class="navbar-nav mr-auto justify-content-center">
                <li class="nav-item">
                    {{ link_to('tracks', 'class': 'nav-link', 'Main') }}
                </li>
                {% if logged_in['role'] === 'admin' %}
                    <li class="nav-item">
                        {{ link_to('admin', 'class': 'nav-link', 'Admin') }}
                    </li>
                    <li class="nav-item dropdown">
                        {{ link_to('#', 'Users',
                                    'class': 'nav-link dropdown-toggle',
                                    'id': 'navbarDropdown',
                                    'data-toggle': 'dropdown') }}

                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            {{ link_to('users', 'class': 'dropdown-item', 'All users') }}
                            {{ link_to('admin/laters', 'class': 'dropdown-item', 'Late users') }}
                        </div>
                    </li>
                    <li class="nav-item">
                        {{ link_to('holidays', 'class': 'nav-link', 'Holidays') }}
                    </li>
                    <li class="nav-item">
                        {{ link_to('admin/settings', 'class': 'nav-link', 'Settings') }}
                    </li>
                {% endif %}
            </ul>
        <div class="nav-item dropdown my-2 my-sm-0">
            {{ link_to('#', 'Account',
                'class': 'btn btn-outline-secondary dropdown-toggle',
                'id': 'navbarDropdown',
                'data-toggle': 'dropdown') }}

            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                {{ link_to('session/changepassword', 'class': 'dropdown-item', 'Change password') }}
                {{ link_to('session/deleteuser', 'class': 'dropdown-item', 'Delete user') }}
            </div>
        </div>&nbsp;&nbsp;
    {% endif %}
    </div>

    </div>
    {%- if logged_in is defined and not(logged_in is empty) -%}
        {{ link_to('session/logout', 'class': 'btn btn-outline-info my-2 my-sm-0', 'Logout') }}
    {% else %}
        {{ link_to('session/login', 'class': 'btn btn-outline-info my-2 my-sm-0', 'Login') }}
    {% endif %}
</nav>

<div class="main-container">
  {{ content() }}
</div>