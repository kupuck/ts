{{ content() }}

{{ form() }}
<div class="card container col-3" style="position: center;">
    <div class="card-header row">
        <h4>Log In</h4>
    </div>
    <div class="card-body">
        <div class="form-group">
            {{ form.render('username') }}
        </div>
        <div class="form-group">
            {{ form.render('password') }}
        </div>
        {{ form.render('Submit') }}
    </div>
</div>
</form>