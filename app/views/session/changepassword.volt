{{ content() }}

{{ form() }}
<div class="card container col-3" style="position: center;">
    <div class="card-header row">
        <h4>Change password</h4>
    </div>
    <div class="card-body">
        <div class="form-group">
            {{ form.render('password') }}
        </div>
        <div class="form-group">
            {{ form.render('confirm') }}
        </div>
        {{ form.render('Change') }}
    </div>
</div>
</form>