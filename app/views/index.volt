<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tracking system</title>
    {{ stylesheet_link('css/additional.css') }}
    {{ stylesheet_link('css/bootstrap.min.css') }}
    {{ stylesheet_link('css/font-awesome.min.css') }}
</head>
<body>
{{ javascriptInclude('js/jquery-3.3.1.min.js') }}
{{ javascriptInclude('js/bootstrap.min.js') }}

{{ content() }}

</body>
</html>