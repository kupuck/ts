{{ content() }}

{{ form() }}
<div class="card container col-3" style="position: center;">
    <div class="card-header row">
        <h4>Create new User</h4>
    </div>
    <div class="card-body">
        <div class="form-group">
            {{ form.render('username') }}
        </div>
        <div class="form-group">
            {{ form.render('password') }}
        </div>
        <div class="form-group">
            {{ form.render('confirm') }}
        </div>
        <div class="form-group">
            {{ form.render('email') }}
        </div>
        <div class="form-group">
            {{ form.render('role', ["class": "form-control"]) }}
        </div>
        {{ form.render('Save') }}
    </div>
</div>
</form>