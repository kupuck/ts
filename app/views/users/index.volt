{{ content() }}


<div class="card container col-6">
    <div class="card-header row">
        <h4>Users</h4>&nbsp;&nbsp;&nbsp;&nbsp;
        {{ link_to("users/create", "class": "btn btn-outline-dark btn-sm", 'Add new one') }}
    </div>

    <div class="card-body">
        {% for user in users %}
            {% if loop.first %}
                <table class="table table-sm table-hover" style="text-align: center;">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Username</th>
                    <th>Email</th>
                    <th>Role</th>
                    <th>Active</th>
                    <th>Update</th>
                    <th>Delete</th>
                </tr>
                </thead>
            {% endif %}
            <tbody>
            <tr>
                <td>{{ user.id }}</td>
                <td>{{ user.username }}</td>
                <td>{{ user.email }}</td>
                <td>{{ user.role }}</td>
                <td>
                    <input type="checkbox"
                           class="user_activity_checkbox"
                           id="active_{{ user.id }}"
                           {% if user.active == 1 %} checked {% endif %}>
                </td>
                <td>
                    {{ link_to("users/edit/" ~ user.id,
                        '<i class="fa fa-pencil" aria-hidden="true"></i>',
                        "class": "btn btn-sm btn-outline-warning") }}
                </td>
                <td>
                    {{ link_to("users/delete/" ~ user.id,
                        '<i class="fa fa-times" aria-hidden="true"></i>',
                        "class": "btn btn-sm btn-outline-danger") }}
                </td>
            </tr>
            {% if loop.last %}
                </tbody>
                </table>
            {% endif %}
        {% endfor %}
    </div>
</div>

{{ javascriptInclude('js/activate_user.js') }}