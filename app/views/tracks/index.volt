{{ content() }}

<form action="javascript:0">
    <div class="card container">
        <div class="card-header row">
            <h4>Tracks</h4>&nbsp;&nbsp;&nbsp;&nbsp;
            <button id="time_{{ logged_in['id'] }}" class="btn btn-outline-success btn-sm time_button">
                {{ btn_text }}
            </button>

            &nbsp;&nbsp;&nbsp;&nbsp;
            <div class>
                <select class="custom-select" id="year_select">
                    {% for item in years %}
                        <option value="{{ item['year'] }}" {% if item['year'] in current_year %} selected {% endif %}>
                            {{ item['year'] }}
                        </option>
                    {% endfor %}
                </select>
            </div>
            &nbsp;&nbsp;
            <div class>
                <select class="custom-select" id="month_select">
                    {% for month in months %}
                        <option value="{{ month['number'] }}" {% if month['number'] in current_month %} selected {% endif %}>
                            {{ month['name'] }}
                        </option>
                    {% endfor %}
                </select>
            </div>

            <div class="month-data">
                <strong>You have:</strong> {{ month_data['total'] }}
                <strong>Assign:</strong> {{ month_data['assigned'] }}
                <strong>Fails:</strong>
                {% if month_data['fails'] !== '0:0' %}
                    <span class="fails-text">{{ month_data['fails'] }}</span>
                {% else %}
                    {{ month_data['fails'] }}
                {% endif %}
            </div>

        </div>
        <div class="card-body">
            <table class="table table-bordered table-sm table-responsive" style="text-align: center;">
                <thead>
                <tr>
                    <th>
                        <a href="javascript:void(0)" id="hideShow" class="btn btn-sm btn-outline-secondary">
                            <i class="fa fa-eye" aria-hidden="true"></i>
                        </a>
                    </th>
                    {% for user in users %}
                        <th scope="col" id="{{ user['id'] }}">{{ user['username'] }}</th>
                    {% endfor %}
                </tr>
                </thead>
                <tbody>
                {% for i in 1..users[0]['tracks']|length-1 %}
                    {% if loop.first %}
                        <tr class="current_day">
                            <td>
                                <p>{{ user['tracks'][i-1]['day_numb'] }}</p>
                                <p>{{ user['tracks'][i-1]['day'] }}</p>
                            </td>
                            {% for user in users %}
                                <td id="times_{{ user['id'] }}_{{ user['tracks'][i-1]['id'] }}">
                                    <p>
                                        {% if loop.first %}
                                            <input type="checkbox" class="lunch"
                                                   id="lunch_{{ user['tracks'][i-1]['id'] }}"
                                                    {% if user['tracks'][i-1]['lunch'] == 1  %} checked {% endif %}
                                            >
                                        {% endif %}
                                    </p>
                                    {% for times in user['tracks'][i-1]['times'] %}
                                        <p>
                                            {{ times['start_time'] }}
                                            {{ times['end_time'] }}
                                        </p>
                                    {% endfor %}
                                    <span>
                                        {% if user['tracks'][i-1]['total'] !== '00:00'  %}
                                            <h6>Total: {{ user['tracks'][i-1]['total'] }}</h6>
                                        {% endif %}
                                    </span>
                                    <span>
                                        {% if user['tracks'][i-1]['less'] !== '00:00' and user['tracks'][i-1]['less'] !== '09:00' %}
                                            <h6>Less: {{ user['tracks'][i-1]['less'] }}</h6>
                                        {% endif %}
                                    </span>
                                    <span>
                                        {% if user['tracks'][i-1]['late'] !== '00:00' %}
                                            <h6 class="fails-text">Fail: {{ user['tracks'][i-1]['late'] }}</h6>
                                        {% endif %}
                                    </span>
                                </td>
                            {% endfor %}
                        </tr>
                    {% endif %}
                    <tr class="other_days">
                        <td>
                            <p>{{ user['tracks'][i]['day_numb'] }}</p>
                            <p>{{ user['tracks'][i]['day'] }}</p>
                        </td>
                        {% for user in users %}
                            <td id="times_{{ user['id'] }}_{{ user['tracks'][i]['id'] }}">
                                {% for times in user['tracks'][i]['times'] %}
                                    <p>
                                        {{ times['start_time'] }}
                                        {{ times['end_time'] }}
                                    </p>
                                {% endfor %}

                                <span>
                                    {% if user['tracks'][i]['total'] !== '00:00'  %}
                                        <h6>Total: {{ user['tracks'][i]['total'] }}</h6>
                                    {% endif %}
                                </span>
                                <span>
                                    {% if user['tracks'][i]['less'] !== '00:00' and user['tracks'][i]['less'] !== '09:00'  %}
                                        <h6>Less: {{ user['tracks'][i]['less'] }}</h6>
                                    {% endif %}
                                </span>
                                <span>
                                    {% if user['tracks'][i]['late'] !== '00:00' %}
                                        <h6 class="fails-text">Fail: {{ user['tracks'][i]['late'] }}</h6>
                                    {% endif %}
                                </span>
                            </td>
                        {% endfor %}
                    </tr>
                {% endfor %}
                </tbody>
            </table>

        </div>
    </div>
</form>


{{ javascriptInclude('js/get_months_by_year.js') }}
{{ javascriptInclude('js/get_data_by_month.js') }}
{{ javascriptInclude('js/control_time.js') }}
{{ javascriptInclude('js/hide_show.js') }}
{{ javascriptInclude('js/lunch.js') }}