<?php
namespace Tracking\Auth;

use Phalcon\Mvc\User\Component;
use Tracking\Models\Users;

class Auth extends Component
{
    public function check($credentials)
    {
        $user = Users::findFirst(
            [
                'username = :username:',
                'bind' => ['username' => $credentials['username']]
            ]
        );

        if ($user == false) {
            throw new Exception('Wrong username/password combination');
        }

        if (!$this->security->checkHash($credentials['password'], $user->password)) {
            throw new Exception('Wrong username/password combination');
        }

        $this->checkUserFlags($user);

        $this->session->set('auth-identity', [
            'id' => $user->id,
            'username' => $user->username,
            'role' => $user->role
        ]);
    }

    public function checkUserFlags(Users $user)
    {
        if ($user->active != '1') {
            throw new Exception('The user is inactive');
        }
}

    public function getIdentity()
    {
        return $this->session->get('auth-identity');
    }

    public function getName()
    {
        $identity = $this->session->get('auth-identity');
        return $identity['name'];
    }

    public function remove()
    {
        $this->session->remove('auth-identity');
    }

    public function authUserById($id)
    {
        $user = Users::findFirstById($id);
        if ($user == false) {
            throw new Exception('The user does not exist');
        }

        $this->checkUserFlags($user);

        $this->session->set('auth-identity', [
            'id' => $user->id,
            'username' => $user->username,
            'role' => $user->role
        ]);
    }

    public function getUser()
    {
        $identity = $this->session->get('auth-identity');
        if (isset($identity['id'])) {

            $user = Users::findFirstById($identity['id']);
            if ($user == false) {
                throw new Exception('The user does not exist');
            }

            return $user;
        }

        return false;
    }
}
