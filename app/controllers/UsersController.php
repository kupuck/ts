<?php

namespace Tracking\Controllers;

use Tracking\Forms\UserForm;
use Tracking\Models\Users;

class UsersController extends ControllerBase
{
    public function initialize()
    {
        $this->view->setVar('logged_in', $this->auth->getIdentity());
        $this->view->setTemplateBefore('public');
    }

    public function indexAction()
    {
        $users = Users::find();
        $this->view->users = $users;
    }

    public function createAction()
    {
        $form = new UserForm();

        //надо добавить CSRF проверку
        try {
            if($this->request->isPost()) {
                if($form->isValid($this->request->getPost()) == false)
                    foreach($form->getMessages() as $msg)
                        $this->flash->error($msg);
                else {
                    $user = new Users([
                        'username' => $this->request->getPost('username'),
                        'password' => $this->security->hash($this->request->getPost('password')),
                        'email' => $this->request->getPost('email'),
                        'role' => $this->request->getPost('role'),
                    ]);
                    if(!$user->save())
                        $this->flash->error($user->getMessages());
                    else {
                        $this->flash->success("User was created successfully");
                        $form->clear();
                    }
                }
            }
        }
        catch(\Exception $ex) {
            $this->flash->error($ex->getMessage());
        }

        $this->view->form = $form;
    }

    public function editAction($id)
    {
        $user = Users::findFirstById($id);

        if(!$user) {
            $this->flash->error('The user was not found');

            return $this->dispatcher->forward(['action' => 'index']);
        }

        if($this->request->isPost()) {
            $user->assign([
                'username' => $this->request->getPost('username'),
                'password' => $this->security->hash($this->request->getPost('password')),
                'email' => $this->request->getPost('email'),
                'role' => $this->request->getPost('role')
            ]);

//            print_die($user->toArray());

            $form = new UserForm($user, ['edit' => true]);

            if($form->isValid($this->request->getPost()) == false)
                foreach($form->getMessages() as $msg)
                    $this->flash->error($msg);
            else {
                if(!$user->save())
                    $this->flash->error($user->getMessages());
                else {
                    $this->flash->success('User was updated successfully');
                    $form->clear();
                    $this->response->redirect('users/index');
                }
            }
        }

        $this->view->user = $user;
        $this->view->form = new UserForm($user, ['edit' => true]);
    }

    public function deleteAction($id)
    {
        $user = Users::findFirstById($id);
        if(!$user) {
            $this->flash->error("User was not found");

            return $this->dispatcher->forward(['action' => 'index']);
        }

        if(!$user->delete())
            $this->flash->error($user->getMessage());
        else
            $this->flash->success("User was deleted");

        $this->response->redirect('users');
//        $this->dispatcher->forward(['controller' => 'users', 'action' => 'index']);
    }

    public function activeAction()
    {
        if($this->request->isPost()) {
            $active = $this->request->getPost('active');

            if($active == '1' || $active == '0') {
                $user = Users::findFirst(
                    [
                        'id = :id:',
                        'bind' => [
                            'id' => $this->request->getPost('user_id')
                        ]
                    ]
                );
                $user->active = $active;

                return json_encode($user->save());
            }
        }

        return json_encode(false);
    }
}