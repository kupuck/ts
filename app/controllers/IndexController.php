<?php
namespace Tracking\Controllers;

class IndexController extends ControllerBase
{
    public function indexAction()
    {
        $this->view->setVar('logged_in', $this->auth->getIdentity());
        $this->view->setTemplateBefore('public');
    }
}
