<?php
namespace Tracking\Controllers;

use Phalcon\Mvc\Controller;
use Phalcon\Mvc\Dispatcher;

class ControllerBase extends Controller
{
    public function beforeExecuteRoute(Dispatcher $dispatcher)
    {
        $controllerName = $dispatcher->getControllerName();

        if ($this->acl->isPrivate($controllerName)) {

            $identity = $this->auth->getIdentity();

            if (!is_array($identity)) {

                $this->flash->notice('You don\'t have access to this module: private');

                $dispatcher->forward([
                    'controller' => 'session',
                    'action' => 'login'
                ]);
                return false;
            }

            $actionName = $dispatcher->getActionName();
            if (!$this->acl->isAllowed($identity['role'], $controllerName, $actionName)) {

                $this->flash->notice('You don\'t have access to this module: ' . $controllerName . ':' . $actionName);

                if ($this->acl->isAllowed($identity['role'], $controllerName, 'index')) {
                    $dispatcher->forward([
                        'controller' => $controllerName,
                        'action' => 'index'
                    ]);
                } else {
                    $dispatcher->forward([
                        'controller' => 'tracks',
                        'action' => 'index'
                    ]);
                }

                return false;
            }
        }
    }
}
