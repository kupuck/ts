<?php

namespace Tracking\Controllers;

use Tracking\Models\Additional;
use Tracking\Models\Tracks;
use Tracking\Models\TracksTime;
use Tracking\Models\Users;
use Tracking\Models\Settings;

class AdminController extends ControllerBase
{
    public function initialize()
    {
        $this->view->setVar('logged_in', $this->auth->getIdentity());
        $this->view->setTemplateBefore('public');
    }

    public function indexAction()
    {
        $this->response->redirect('admin/tracks');
    }

    public function tracksAction($year = null, $month = null)
    {
        if ($year == null)  $year = date('Y');
        if ($month == null) $month = date('m');

        $user_id = $this->auth->getIdentity()['id'];

        if ($user_id) {
            $users = Users::find(
                [
                    'columns' => ['id', 'username'],
                    'order' => 'id = :id: DESC, id ASC',
                    'bind' => ['id' => $user_id]
                ]
            )->toArray();

            $days = Additional::getAllDaysInMonth($month, $year);

            foreach($users as &$user) {
                $user['tracks'] = [];
                $i = 0;

                foreach($days as $day) {
                    $date = "$year-$month-$day[number]";
                    $user['tracks'] = array_merge($user['tracks'], Tracks::getDailyByUser($user['id'], $date));
                    $late = Tracks::getLateTime($user['tracks'][$i]['id']);
                    $less = 9 * 3600 - (int)$user['tracks'][$i]['total'];

                    $user['tracks'][$i] = array_merge($user['tracks'][$i], ['day' => $day['day']]);
                    $user['tracks'][$i] = array_merge($user['tracks'][$i], ['day_numb' => $day['number']]);
                    $user['tracks'][$i] = array_merge($user['tracks'][$i], ['day_numb' => $day['number']]);
                    $user['tracks'][$i] = array_merge($user['tracks'][$i], ['late' => gmdate('H:i', $late)]);
                    $user['tracks'][$i]['less'] = ($less > 0) ? gmdate('H:i', $less) : '0';
                    $user['tracks'][$i]['total'] = gmdate('H:i', $user['tracks'][$i]['total']);

                    foreach ($user['tracks'] as &$track) {
                        $times = TracksTime::findByTrackId($track['id'])->toArray();

                        if(count($times) == 0)
                            $track['times'] = [];
                        else
                            $track['times'] = $times;
                    }
                    $i++;
                }
            }

            $this->view->users = $users;
            $this->view->btn_text = $this->checkUsersIsStopped($users);
            $this->view->months = Tracks::getAvailableMonthsByYear($year);
            $this->view->years = Tracks::getAvailableYears();
            $this->view->month_data = [
                'assigned' => Tracks::getMonthAssigned($month, $year),
                'total' => Tracks::getMonthTotalByUser($user_id, $month, $year),
                'fails' => Tracks::getMonthFailsByUser($user_id, $month, $year)
            ];
        }
    }

    public function changeTimeAction()
    {
        if($this->request->isPost()) {
            $change_time = $this->request->getPost('time');
            $time_id = $this->request->getPost('time_id');
            $action = $this->request->getPost('action');
            $errors = [];

            if(!is_numeric($time_id))
                $errors[] = 'The ID of track time is not correct';

            if(!Additional::isTime($change_time))
                $errors[] = 'The time is not correct';

            if(count($errors) == 0) {
                $time = TracksTime::findFirst([
                    'id = :id:',
                    'bind' => ['id' => $time_id]
                ]);

                if($action == 'start') {
                    $difference = strtotime($time->end_time) - strtotime($change_time);

                    if($difference > 0) {
                        $time->start_time = $change_time;
                        $time->save();

                        return json_encode(Tracks::updateTotal($time->track_id));
                    }

                    $errors[] = 'Start time can not be bigger than end time';
                }
                else {
                    $difference = strtotime($change_time) - strtotime($time->start_time);

                    if($difference > 0) {
                        $time->end_time = $change_time;
                        $time->save();

                        return json_encode(Tracks::updateTotal($time->track_id));
                    }

                    $errors[] = 'End time can not be smaller than start time';
                }
            }

            return json_encode($errors);
        }

        return json_encode(false);
    }

    public function settingsAction()
    {
        $settings = Settings::find()->toArray();
        $this->view->settings = $settings;
    }

    //cwt - change work time
    public function cwtAction()
    {
        if($this->request->isPost()) {
            $errors = [];
            $time = $this->request->getPost('time');

            if(!isset($time))
                $errors[] = 'There is no change of work start time';
            if(!Additional::isTime($time))
                $errors[] = 'Changed time is not valid';

            if(count($errors) == 0) {
                $settings = Settings::findFirst([
                    "name = 'work_start_time'"
                ]);
                $settings->value = $time;

                if($settings->save())
                    return json_encode(true);

                $errors[] = 'Work start time was not changed';
            }

            return json_encode($errors);
        }

        return json_encode(false);
    }

    public function latersAction($date = null)
    {
        $users = Users::getLateUsers($date);

        $this->view->users = $users;
    }

    private function checkUsersIsStopped($users)
    {
        $result = [];

        foreach($users as $user) {
            $stopped = Tracks::isStopped($user['id']);
            $result[] = (is_numeric($stopped) || $stopped['stop']) ? 'Start' : 'Stop';
        }

        return $result;
    }
}