<?php

namespace Tracking\Controllers;

use Tracking\Models\Additional;
use Tracking\Models\TracksTime;
use Tracking\Models\Users;
use Tracking\Models\Tracks;

class TracksController extends ControllerBase
{
    public function initialize()
    {
        $this->view->setVar('logged_in', $this->auth->getIdentity());
        $this->view->setTemplateBefore('public');
    }

    public function indexAction($month = null, $year = null)
    {
        if ($month == null) $month = date('m');
        if ($year == null)  $year = date('Y');

        $user_id = $this->auth->getIdentity()['id'];

        if ($user_id) {
            $users = Users::find([
                'active = 1',
                'columns' => ['id', 'username'],
                'order' => 'id = :id: DESC, id ASC',
                'bind' => ['id' => $user_id]
            ])->toArray();

            $days = Additional::getAllDaysInMonth($month, $year);

            foreach($users as &$user) {
                $user['tracks'] = [];
                $i = 0;

                foreach($days as $day) {
                    $date = "$year-$month-$day[number]";
                    $user['tracks'] = array_merge($user['tracks'], Tracks::getDailyByUser($user['id'], $date));
                    $late = Tracks::getLateTime($user['tracks'][$i]['id']);
                    $less = 9 * 3600 - (int)$user['tracks'][$i]['total'];

                    $user['tracks'][$i] = array_merge($user['tracks'][$i], ['day' => $day['day']]);
                    $user['tracks'][$i] = array_merge($user['tracks'][$i], ['day_numb' => $day['number']]);
                    $user['tracks'][$i] = array_merge($user['tracks'][$i], ['day_numb' => $day['number']]);
                    $user['tracks'][$i] = array_merge($user['tracks'][$i], ['late' => gmdate('H:i', $late)]);
                    $user['tracks'][$i]['less'] = ($less > 0) ? gmdate('H:i', $less) : '0';
                    $user['tracks'][$i]['total'] = gmdate('H:i', $user['tracks'][$i]['total']);


                    foreach ($user['tracks'] as &$track) {
                        $times = TracksTime::findByTrackId($track['id'])->toArray();

                        if(count($times) == 0)
                            $track['times'] = [];
                        else
                            $track['times'] = $times;
                    }
                    $i++;
                }
            }

            $stopped = Tracks::isStopped($user_id);
            $btn_text = (is_numeric($stopped) || $stopped['stop']) ? 'Start' : 'Stop';

            $this->view->users = $users;
            $this->view->btn_text = $btn_text;
            $this->view->current_year = $year;
            $this->view->current_month = $month;
            $this->view->years = Tracks::getAvailableYears();
            $this->view->months = Tracks::getAvailableMonthsByYear($year);
            $this->view->month_data = [
                'assigned' => Tracks::getMonthAssigned($month, $year),
                'total' => Tracks::getMonthTotalByUser($user_id, $month, $year),
                'fails' => Tracks::getMonthFailsByUser($user_id, $month, $year)
            ];
        }
    }

    public function lunchAction()
    {
        if($this->request->isPost()) {
            $lunch = $this->request->getPost('lunch');
            if($lunch == '1' || $lunch == '0') {
                $track = Tracks::findFirst(
                    [
                        'id = :track_id:',
                        'bind' => [
                            'track_id' => $this->request->getPost('track_id')
                        ]
                    ]
                );
                $lunchHour = ($lunch == '1') ? -3600 : 3600;
                $track->total = (int)$track->total + $lunchHour;
                $track->lunch = $lunch;
                $track->save();
            }
        }
    }

    public function timeAction()
    {
        $user_role = $this->auth->getIdentity()['role'];
        $user_id = $this->request->getPost('user_id');

        if(!$user_id || $user_role != 'admin')
            $user_id = $this->auth->getIdentity()['id'];


        if($user_id && $this->request->isPost()) {
            $check = Tracks::isStopped($user_id);

            if(is_numeric($check))
                $result = Tracks::startTime($check);
            else {
                if($check['stop'])
                    $result = Tracks::startTime($check['track_id']);
                else
                    $result = Tracks::stopTime($check['track_id']);
            }

            return json_encode($result);
        }
    }

    public function getMonthsByYearAction()
    {
        if($this->request->isPost()) {
            $year = $this->request->getPost('year');

            if(isset($year) && is_numeric($year))
                return json_encode(Tracks::getAvailableMonthsByYear($year));
        }

        return json_encode(false);
    }
}