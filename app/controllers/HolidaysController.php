<?php

//Надо сделать что нибудь с чекбоксом
namespace Tracking\Controllers;

use Tracking\Forms\HolidayForm;
use Tracking\Models\Holidays;

class HolidaysController extends ControllerBase
{
    public function initialize()
    {
        $this->view->setVar('logged_in', $this->auth->getIdentity());
        $this->view->setTemplateBefore('public');
    }

    public function indexAction()
    {
        $holidays = Holidays::find();
        $this->view->setVar('holidays', $holidays);
    }

    public function createAction()
    {
        $form = new HolidayForm();

        try {
            if($this->request->isPost()) {
                if($form->isValid($this->request->getPost()) == false)
                    foreach($form->getMessages() as $msg)
                        $this->flash->error($msg);
                else {
                    $holiday = new Holidays([
                        'name' => $this->request->getPost('name'),
                        'date' => $this->request->getPost('date')
                    ]);
                    if(!$holiday->save())
                        $this->flash->error($holiday->getMessages());
                    else {
                        $this->flash->success("Holiday was created successfully");
                        $form->clear();
                    }
                }
            }
        }
        catch(\Exception $ex) {
            $this->flash->error($ex->getMessage());
        }

        $this->view->form = $form;
    }

    public function editAction($id)
    {
        $holiday = Holidays::findFirstById($id);

        if(!$holiday) {
            $this->flash->error('The Holiday was not found');

            return $this->dispatcher->forward(['action' => 'index']);
        }

        if($this->request->isPost()) {
            $holiday->assign([
                'name' => $this->request->getPost('name'),
                'date' => $this->request->getPost('date')
            ]);

            $form = new HolidayForm($holiday, ['edit' => true]);

            if($form->isValid($this->request->getPost()) == false)
                foreach($form->getMessages() as $msg)
                    $this->flash->error($msg);
            else {
                if(!$holiday->save())
                    $this->flash->error($holiday->getMessages());
                else {
                    $this->flash->success('Holiday was updated successfully');
                    $form->clear();
                    $this->response->redirect('holidays/index');
                }
            }
        }

        $this->view->holiday = $holiday;
        $this->view->form = new HolidayForm($holiday, ['edit' => true]);
    }

    public function deleteAction($id)
    {
        $holiday = Holidays::findFirstById($id);
        if(!$holiday) {
            $this->flash->error("Holiday was not found");

            return $this->dispatcher->forward(['action' => 'index']);
        }

        if(!$holiday->delete())
            $this->flash->error($holiday->getMessage());
        else
            $this->flash->success("Holiday was deleted");

        $this->response->redirect('holidays');
    }

    public function everyYearAction()
    {
        if($this->request->isPost()) {
            $every_year = $this->request->getPost('every_year');

            if($every_year == '1' || $every_year == '0') {
                $holiday = Holidays::findFirst(
                    [
                        'id = :id:',
                        'bind' => [
                            'id' => $this->request->getPost('holiday_id')
                        ]
                    ]
                );
                $holiday->every_year = $every_year;

                return json_encode($holiday->save());
            }
        }

        return json_encode(false);
    }

}