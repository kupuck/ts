<?php

namespace Tracking\Controllers;

use Tracking\Forms\LoginForm;
use Tracking\Forms\ChangePasswordForm;
use Tracking\Auth\Exception as AuthException;
use Tracking\Models\Users;

class SessionController extends ControllerBase
{
    public function initialize()
    {
        if($this->auth->getIdentity())
            $this->view->setVar('logged_in', $this->auth->getIdentity());

        $this->view->setTemplateBefore('public');
    }
    public function indexAction()
    {
    }

    public function loginAction()
    {
        $form = new LoginForm();

        //CSRF
        try {
            if($this->request->isPost()) {
                if($form->isValid($this->request->getPost()) == false)
                    foreach($form->getMessages() as $msg)
                        $this->flash->error($msg);
                else {
                    $this->auth->check(
                        [
                            'username' => $this->request->getPost('username'),
                            'password' => $this->request->getPost('password')
                        ]
                    );

                    return $this->response->redirect('tracks');
                }
            }
        }
        catch(AuthException $ex) {
            $this->flash->error($ex->getMessage());
        }

        $this->view->form = $form;
    }

    public function logoutAction()
    {
        $this->auth->remove();

        return $this->response->redirect('index');
    }

    public function changePasswordAction()
    {
        $form = new ChangePasswordForm();

        if ($this->request->isPost()) {
            if (!$form->isValid($this->request->getPost())) {
                foreach ($form->getMessages() as $message) {
                    $this->flash->error($message);
                }
            }
            else {
                $user = Users::findFirstById($this->auth->getIdentity()['id']);
                $user->password = $this->security->hash($this->request->getPost('password'));

                if (!$user->save()) {
                    $this->flash->error($user->getMessages());
                }
                else {
                    $this->flash->success('Your password was successfully changed');
                    $this->dispatcher->forward([
                        'controller' => 'tracks',
                        'action' => 'index'
                    ]);
                }
            }
        }

        $this->view->form = $form;
    }

    public function deleteUserAction()
    {
        $user = Users::findFirstById($this->auth->getIdentity()['id']);
        if(!$user) {
            $this->flash->error("User was not found");

            return $this->dispatcher->forward(['action' => 'index']);
        }
        $user->active = '0';

        if(!$user->save())
            $this->flash->error($user->getMessage());
        else {
            $this->auth->remove();
            $this->flash->success("User was deleted");

            $this->dispatcher->forward([
                'controller' => 'index',
                'action' => 'index'
            ]);
        }
    }
}