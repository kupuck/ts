<?php

use Phalcon\Config;
use Phalcon\Logger;

return new Config([
    'privateResources' => [
//        'tracks' => [
//            'index',
//            'time',
//            'lunch',
//        ],
        'holidays' => [
            'index',
            'create',
            'edit',
            'delete',
            'everyyear'
        ],
        'users' => [
            'index',
            'edit',
            'create',
            'delete',
            'active'
        ],
        'admin' => [
            'index',
            'tracks',
            'laters',
            'changetime',
            'settings',
            'cwt'
        ]
    ]
]);
