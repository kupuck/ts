$('.every_year_checkbox').on('change', function() {
        var holiday_id = (this.id).substr(11);
        if(isNaN(holiday_id))
            return false;

        var data = {
            holiday_id: holiday_id,
            every_year: this.checked ? 1 : 0
        };

        $.ajax({
            type: "POST",
            data: data,
            url: "http://ts/holidays/everyyear"
        });
    }
);