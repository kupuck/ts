$(function() {
    $(document).ready(function() {
        $('.other_days').hide();
    });

    $(this).on('click', '#hideShow', function() {
        if($('.other_days').is(':hidden')) {
            $('.other_days').show();
            $('#hideShow').html('<i class="fa fa-eye-slash" aria-hidden="true"></i>');
        }
        else {
            $('.other_days').hide();
            $('#hideShow').html('<i class="fa fa-eye" aria-hidden="true"></i>');
        }
    });
});