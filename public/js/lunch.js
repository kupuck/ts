$('.lunch').on('change', function() {
        var track_id = (this.id).substr(6);
        if(isNaN(track_id))
            return false;

        var data = {
            track_id: track_id,
            lunch: this.checked ? 1 : 0
        };

        $.ajax({
            type: "POST",
            data: data,
            url: "http://ts/tracks/lunch"
        });
    }
);