$('#work_start_time').on('change', function() {
    var time = this.value;

    $.ajax({
        type: 'POST',
        data: {time: time},
        url: 'http://ts/admin/cwt',
        success: function(response) {
            response = JSON.parse(response);

            if((typeof response) == 'object') {
                var result = '<div class="alert alert-danger alert-dismissible">' +
                    '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>';

                for(i = 0; i < response.length; i++)
                    result += '<p>' + i + ') ' + response[i] + '</p>';

                result += '</div>';
                $(result).insertBefore('.main-container');
            }
        }
    });
});