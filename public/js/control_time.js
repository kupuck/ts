$('.time_button').on('click', function() {
    var data = {
        user_id: (this.id).substr(5)
    };

    $.ajax({
        type: "POST",
        data: data,
        url: "http://ts/tracks/time",
        success: function(response) {
            response = JSON.parse(response);

            var track = '#times_' + response.user_id + '_' + response.track_id;
            if(response.stop === true) {
                $('#time_' + response.user_id).html('Start');

                $(track + ' p:last').append(response.time);
                $(track + ' span:first').html('Total: ' + response.total);
            }
            else {
                $('<p>' + response.time + ' </p>').insertBefore(track + ' span:first');
                $('#time_' + response.user_id).html('Stop');
            }
        }
    });
});