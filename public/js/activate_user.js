$('.user_activity_checkbox').on('change', function() {
        var user_id = (this.id).substr(7);
        if(isNaN(user_id))
            return false;

        var data = {
            user_id: user_id,
            active: this.checked ? 1 : 0
        };

        $.ajax({
            type: "POST",
            data: data,
            url: "http://ts/users/active"
        });
    }
);