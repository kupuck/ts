$('#year_select').on('change', function() {

    data = {
        year: $(this).find(':selected').val()
    };

    $.ajax({
        type: 'POST',
        data: data,
        url: 'http://ts/tracks/getmonthsbyyear',
        success: function(response) {
            response = JSON.parse(response);

            if(response) {
                $('#month_select').find('option').remove();

                for(i = 0; i < response.length; i++) {
                    var option = '<option value="' + response[i].number + '">' + response[i].name + '</option>';

                    $('#month_select').append(option);
                }
            }
        }
    });
});