$("input[type='text']").on('change', function() {
    var data = {
        time_id: (this.id).substr((this.id).indexOf('time_')+5),
        time: this.value,
        action: (this.id).substr(0, (this.id).indexOf('_'))
    };

    $.ajax({
        type: 'POST',
        data: data,
        url: 'http://ts/admin/changetime',
        success: function(response) {
            response = JSON.parse(response);

            if((typeof response) == 'object') {
                var result = '<div class="alert alert-danger alert-dismissible">' +
                    '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>';

                for(i = 0; i < response.length; i++)
                    result += '<p>' + i + ') ' + response[i] + '</p>';

                result += '</div>';
                $(result).insertBefore('.main-container');
            }
        }
    });
});